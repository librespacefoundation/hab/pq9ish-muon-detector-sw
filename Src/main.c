/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "can.h"
#include "dma.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <string.h>
#include"ds18b20.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef struct muon {
	uint8_t value;
	uint32_t time_from_previous;
}muon;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define MUON_BOARD_ID 0x02 << 6
#define MUON_PACKET_ID 0x00
#define FREQUENCY_PACKET_ID 0x01
#define TEMPERATURE_PACKET_ID 0x02

#define MUONS_PER_PACKET 2
#define FREQUENCY_PER 2 //100


#define FULL_ASSERT


/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

//CAN_HandleTypeDef     CanHandle;
CAN_TxHeaderTypeDef   TxHeader;
//CAN_RxHeaderTypeDef   RxHeader;
uint8_t               TxData[8];
//uint8_t               RxData[8];
uint32_t              TxMailbox;

float temperature;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void CAN_send_muon(muon muon);
void CAN_send_frequency(uint32_t freq);
void CAN_send_temperature();

uint32_t compute_frequency(muon* muons);
//void write_to_packet(uint32_t * byte,uint8_t level,uint32_t time);
static void canSetup(void);

void tobits(char* input);
float gettemp(char* input);
void DS18B20_OnComplete(void);
void DS18B20_Error(void);
void get_temp();

void blinkled(){
	  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET ); //turn led on
	  HAL_Delay(100);//wait for circuit to stabilize
	  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET ); //turn led on
	  HAL_Delay(100);//wait for circuit to stabilize
	  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET ); //turn led on
	  HAL_Delay(100);//wait for circuit to stabilize
	  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET ); //turn led on
	  HAL_Delay(100);//wait for circuit to stabilize
}



/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
const int SIGNAL_THRESHOLD    =28; //450;this was for 12bit accuracy        // Min threshold to trigger on
const int RESET_THRESHOLD     =26; //420;

uint8_t value;

uint32_t time ;
uint32_t old_time ;
uint32_t uptime;

uint8_t i=0; //counter for frequency computation and uptime
uint8_t j=0; //counter for muon packet
uint32_t frequency = 0;

struct muon muons[FREQUENCY_PER];
uint32_t packet[MUONS_PER_PACKET];

char buff[10] = {0,0,0,0,0,0,0,0,0,0};//ds18b20 sensor





/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC_Init();
  MX_CAN_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */
  HAL_Delay(1000);//wait for circuit to stabilize

  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1,GPIO_PIN_SET ); //enable high voltage on sensor
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET ); //turn led off

  HAL_ADC_Start(&hadc);
  HAL_Delay(1000);//wait for circuit to stabilize

  canSetup();

  OneWire_SetCallback(DS18B20_OnComplete, DS18B20_Error);


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {


	  if(i >= FREQUENCY_PER){
		  frequency = compute_frequency(muons); // measured in mHz
		  uptime = HAL_GetTick();
		  //create can packet with frequency and uptime
		  //and send them over can

		  CAN_send_frequency(frequency);
		  CAN_send_temperature();//here only for testing... should be implemented with a timer
		  i=0;
	  }
	  if (j >=MUONS_PER_PACKET){
		  //send packet over can
		  j=0;
	  }
	  HAL_ADC_PollForConversion (&hadc, 1000);
	  value = HAL_ADC_GetValue (&hadc);
	  if(value>SIGNAL_THRESHOLD){

		  blinkled();

		  time = HAL_GetTick();
		  muons[i].value = value;
		  muons[i].time_from_previous = (uint32_t) (time-old_time);
		  CAN_send_muon(muons[i++]);
		  //packet[j].value = value;
		  //packet[j++].time_from_previous = (uint32_t) (time-old_time);
		  //write_to_packet(&packet[j],value,time-old_time);
		  //j++;
		  old_time =time;
		  while(value>RESET_THRESHOLD){
			  HAL_ADC_PollForConversion (&hadc, 1000);
			  value = HAL_ADC_GetValue (&hadc);
		  }
			  //HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET );
			  //HAL_Delay(3);
			  //HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5,GPIO_PIN_RESET );

	  }

		//HAL_CAN_AddTxMessage()

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI14|RCC_OSCILLATORTYPE_HSI48;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.HSI14State = RCC_HSI14_ON;
  RCC_OscInitStruct.HSI14CalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI48;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL5;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV6;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
uint32_t compute_frequency(muon* muons){ //frequency in mHz
	uint32_t sum = 0;
	for (int i=0; i<FREQUENCY_PER;i++){
		sum += muons[i].time_from_previous;
	}
	return (uint32_t) (FREQUENCY_PER*1000000/sum);
}
/*
void write_to_packet(uint32_t * byte,uint8_t level,uint32_t time){
	uint32_t result = (uint32_t) level;
	result = result<<3*8;
	time = time & 0x8F;
	result = result | time;
	*byte = result;
	return;
}
*/

void CAN_send_muon(muon muon){
	//CAN_TxHeaderTypeDef TxHeader; probably not needed,defined globally
	TxHeader.StdId = MUON_BOARD_ID | MUON_PACKET_ID ;
	TxHeader.ExtId = 0;				//dummy value - not using extid
	TxHeader.IDE = CAN_ID_STD; 		// choose between 11 or 29 bit ID - choosing STD (11 bit)
	TxHeader.RTR = CAN_RTR_DATA; 	// signifies a data frame
	TxHeader.DLC = sizeof(muon.value)+sizeof(muon.time_from_previous); 				// length of message in bytes
	TxHeader.TransmitGlobalTime = DISABLE;

	memcpy( &(TxData[0]), &(muon.value), sizeof(muon.value)); // see README about the packet oraganization
	memcpy(&(TxData[sizeof(muon.value)]), &(muon.time_from_previous), sizeof(muon.time_from_previous));


	if (HAL_CAN_AddTxMessage(&hcan, &TxHeader, TxData, &TxMailbox) != HAL_OK) {
	    Error_Handler();
	}
}


void CAN_send_frequency(uint32_t freq){
	TxHeader.StdId = MUON_BOARD_ID | FREQUENCY_PACKET_ID ;
	TxHeader.ExtId = 0;				//dummy value - not using extid
	TxHeader.IDE = CAN_ID_STD; 		// choose between 11 or 29 bit ID - choosing STD (11 bit)
	TxHeader.RTR = CAN_RTR_DATA; 	// signifies a data frame
	TxHeader.DLC = sizeof(freq); 				// length of message in bytes
	TxHeader.TransmitGlobalTime = DISABLE;

	memcpy(TxData, &freq, sizeof(freq));


	if (HAL_CAN_AddTxMessage(&hcan, &TxHeader, TxData, &TxMailbox) != HAL_OK) {
		Error_Handler();
	}
}


void CAN_send_temperature(){

	get_temp();

	TxHeader.StdId = MUON_BOARD_ID | TEMPERATURE_PACKET_ID ;
	TxHeader.ExtId = 0;				//dummy value - not using extid
	TxHeader.IDE = CAN_ID_STD; 		// choose between 11 or 29 bit ID - choosing STD (11 bit)
	TxHeader.RTR = CAN_RTR_DATA; 	// signifies a data frame
	TxHeader.DLC = sizeof(temperature); 				// length of message in bytes
	TxHeader.TransmitGlobalTime = DISABLE;

	memcpy(TxData, &temperature, sizeof(temperature));


	if (HAL_CAN_AddTxMessage(&hcan, &TxHeader, TxData, &TxMailbox) != HAL_OK) {
		Error_Handler();
	}



}


void tobits(char* input){

	char a;
	for(int k = 0; k < 9 ;k++){
		for (int i=7;i>-1;i--){
			if(( input[k] & (1<<i)) != 0  ) a='1';
			else a = '0';
		}
	}
}

float gettemp(char* input){
    uint8_t lsb = input[0];
    uint8_t msb = input[1];
	switch(input[4]){
		case(0x1F)://9bit res
				lsb &= 0xF8;
				break;
		case(0x3F)://10bit res
				lsb &= 0xFC;
				break;
		case(0x5F)://11bit res
				lsb &= 0xFE;
				break;
		case(0x7F)://12bit res
				break;
		default: return -9999;
	}
	uint8_t sign = msb & 0x80;
	int16_t temp = (msb << 8) + lsb;
	if (sign) {
		temp = ((temp ^ 0xffff) + 1) * -1;
	}
	return temp / 16.0;
}

void get_temp(){
	for(int i=0; i<10;i++)buff[i]=0;
	OneWire_Init();
	OneWire_Execute(0xcc,0,0x00,0); /* skip rom phase */
	OneWire_Execute(0xcc,0,0x44,0); /* start to Convert T */
	HAL_Delay(1000); /* Wait to convertion time */
	OneWire_Execute(0xcc,0,0x00,0); /* skip rom phase */
	OneWire_Execute(0xcc,0,0xbe,buff); /* start to read configuration & result */
	HAL_Delay(500);


		//set accuracy to 12 bits

		if(buff[4]!=0x7F){
			buff[4]=0x7F;
			OneWire_Execute(0xcc,0,0x4e,buff[2]);
			HAL_Delay(500);
			for(int i=0; i<10;i++)buff[i]=0;
			  	OneWire_Init();
				OneWire_Execute(0xcc,0,0x00,0); /* skip rom phase */
				OneWire_Execute(0xcc,0,0x44,0); /* start to Convert T */
				HAL_Delay(1000); /* Wait to convertion time */
				OneWire_Execute(0xcc,0,0x00,0); /* skip rom phase */
				OneWire_Execute(0xcc,0,0xbe,buff); /* start to read configuration & result */
				HAL_Delay(500);

		}

		tobits(buff);

		temperature = gettemp(buff);
}


void DS18B20_OnComplete(void){
	HAL_Delay(10);
}
void DS18B20_Error(void){
	HAL_Delay(10);
}





/**
 * Setup CAN Bus
 * copied from atmo sw
 */
static void canSetup(void) {
  CAN_FilterTypeDef sFilterConfig;
  sFilterConfig.FilterBank = 0;
  sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
  sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
  sFilterConfig.FilterIdHigh = 0x0000;
  sFilterConfig.FilterIdLow = 0x0000;
  sFilterConfig.FilterMaskIdHigh = 0x0000;
  sFilterConfig.FilterMaskIdLow = 0x0000;
  sFilterConfig.FilterFIFOAssignment = CAN_RX_FIFO0;
  sFilterConfig.FilterActivation = ENABLE;
  sFilterConfig.SlaveStartFilterBank = 14;

  if (HAL_CAN_ConfigFilter (&hcan, &sFilterConfig) != HAL_OK) {
    /* Filter configuration Error */
    Error_Handler();
  }

  /*##-3- Start the CAN peripheral ###########################################*/
  if (HAL_CAN_Start (&hcan) != HAL_OK) {
    /* Start Error */
    Error_Handler();
  }
  if (HAL_CAN_ActivateNotification (&hcan, CAN_IT_RX_FIFO0_MSG_PENDING)
      != HAL_OK) {
    /* Notification Error */
    Error_Handler();
  }
//  TxHeader.StdId = PQ_ATMO_NODE_STDID;
//  TxHeader.ExtId = PQ_ATMO_NODE_EXTID;
//  TxHeader.RTR = CAN_RTR_DATA;
//  TxHeader.IDE = CAN_ID_STD;
//  TxHeader.DLC = 8;
//  TxHeader.TransmitGlobalTime = DISABLE;
}






/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
	for(int i =0;i<100;i++ ){
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET );
		HAL_Delay(10);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET );
		HAL_Delay(10);
	}

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
