Firmware for this board https://gitlab.com/drid/pq9ish-muon-detector for use high altitude balloons

SW4STM32 project generated with CubeMX

Work in progress...(no CAN functionality yet)


## can messages to be processed by comms board

Can Ids

#define MUON_BOARD_ID 0x02 << 6
#define MUON_PACKET_ID 0x00
#define FREQUENCY_PACKET_ID 0x01
#define TEMPERATURE_PACKET_ID 0x02


* MUON_PACKET (code 0x00) - 5 byte payload 

| byte 0 | bytes 1-4 |
---------------------------------|---------------------------------------------------------------------
| muon level (from 8-bit adc) [dtype = uint8_t ] | time since previous ( in msec) [dtype = uint32_t] |

* FREQUENCY_PACKET (code 0x01) - 4 byte payload

    frequency meazured in mHz [dtype = uint32_t]

* TEMPERATURE_PACKET (code 0x02) 

    temperature in C [dtype = float]

## flashing 

on the first time  BOOT_SEL bit should be set to 0 .
